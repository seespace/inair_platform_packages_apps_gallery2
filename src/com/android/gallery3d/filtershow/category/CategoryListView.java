/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.gallery3d.filtershow.category;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.KeyEvent;
import android.widget.ListView;
import android.util.Log;
import android.graphics.Rect;
import com.android.gallery3d.R;


public class CategoryListView extends ListView {

    private static final String TAG = "CategoryListView";
    private int itemsCount = 0;
    private int currentFocusedItem = 0;
    private CategoryAdapter mAdapter = null;

    public CategoryListView (Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    
    @Override
    public boolean onKeyDown (int keyCode, KeyEvent event){
        int position = (Integer)findFocus().getTag();
        mAdapter = (CategoryAdapter)this.getAdapter();
        itemsCount = mAdapter.getCount();   

        if (keyCode == KeyEvent.KEYCODE_DPAD_UP){
            View v = focusSearch(ViewGroup.FOCUS_UP);
            if (v != null){
                v.requestFocus();
            }
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN){
            View v = focusSearch(ViewGroup.FOCUS_DOWN);
            if (v != null){
                v.requestFocus();
            }
        } 
       
        return super.onKeyDown(keyCode, event);
    }
}
